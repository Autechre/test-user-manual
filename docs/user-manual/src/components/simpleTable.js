import React from "react";
import "./simpleTable.css";

const SimpleTable = ({ headers, rows }) => {
  const renderTableData = () => {
    return rows.map((row) => {
      const { id, name, description } = row; //destructuring
      return (
        <tr key={id}>
          <td>{name}</td>
          <td>{description}</td>
        </tr>
      );
    });
  };

  return (
    <table>
      <tr>
        {headers.map((header) => (
          <th>{header.title}</th>
        ))}
      </tr>
      <tbody>{renderTableData()}</tbody>
    </table>
  );
};

export default SimpleTable;
