import React from "react";
import Layout from "./layout";

const Resources = () => {
  return <Layout>resources</Layout>;
};

export default Resources;
