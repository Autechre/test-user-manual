import React from "react";
import "./tip.css";

const Tip = ({ title, description, version }) => {
  return (
    <div className="tip">
      <p className="tip-title">{title}</p>
      <p className="tip-description">
        <div>
          {description}
          {version ? <p>{version}</p> : null}
        </div>
      </p>
    </div>
  );
};

export default Tip;
