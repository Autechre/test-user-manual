import React from "react";
import Layout from "./layout";
import Card from "./card";
import SimpleTable from "./simpleTable";

const compatibilities = [
  {
    id: 1,
    name: "Mule Runtime",
    description: "4.1.x EE or higher",
  },
  {
    id: 2,
    name: "JIRA",
    description: "6.x or higher",
  },
];

const Prerequisites = () => {
  return (
    <>
      <Layout>
        <Card>
          <h2>Prerequisites</h2>
          <p>
            This document assumes that you are familiar with Mule 4,{" "}
            <a
              className="anchor"
              href="https://docs.mulesoft.com/connectors/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Anypoint Connectors
            </a>
            , and{" "}
            <a
              className="anchor"
              href="https://www.mulesoft.com/lp/dl/studio"
              target="_blank"
              rel="noopener noreferrer"
            >
              Anypoint Studio
            </a>
            . To increase your familiarity with Studio, consider completing a{" "}
            <a
              className="anchor"
              href="https://docs.mulesoft.com/studio/6/basic-studio-tutorial"
              target="_blank"
              rel="noopener noreferrer"
            >
              Anypoint Studio Tutorial
            </a>
            . This page requires some basic knowledge of{" "}
            <a
              className="anchor"
              href="https://docs.mulesoft.com/mule-runtime/latest/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Mule Concepts
            </a>
            ,{" "}
            <a
              className="anchor"
              href="https://docs.mulesoft.com/mule-runtime/latest/mule-components"
              target="_blank"
              rel="noopener noreferrer"
            >
              Components in a Mule Flow
            </a>
            , and{" "}
            <a
              className="anchor"
              href="https://docs.mulesoft.com/mule-runtime/3.8/global-elements"
              target="_blank"
              rel="noopener noreferrer"
            >
              Global Elements
            </a>
            .
          </p>
        </Card>
        <Card>
          <h2>Hardware and Software Requirements</h2>
          <p>
            For hardware and software requirements, please visit the{" "}
            <a
              className="anchor"
              href="https://docs.mulesoft.com/mule-runtime/4.2/hardware-and-software-requirements"
              target="_blank"
              rel="noopener noreferrer"
            >
              Hardware and Software Requirements
            </a>{" "}
            page.
          </p>
        </Card>
        <Card>
          <h2>Compatibility</h2>
          <SimpleTable
            headers={[{ title: "Application/Service" }, { title: "Version" }]}
            rows={compatibilities}
          />
        </Card>
        <Card>
          <h2>How to Install</h2>
          <p>
            You can install the connector in Anypoint Studio using the
            instructions in{" "}
            <a
              className="anchor"
              href="https://docs.mulesoft.com/exchange/#installing-a-connector-from-anypoint-exchange"
              target="_blank"
              rel="noopener noreferrer"
            >
              Installing a Connector from Anypoint Exchange
            </a>
            .
          </p>
        </Card>
        <Card>
          <h2>Upgrading from an Older Version</h2>
          <p>
            If you’re currently using an older version of the connector, a small
            popup appears in the bottom right corner of Anypoint Studio with an
            "Updates Available" message.
          </p>
          <ol className="ol-list">
            <li>Click the popup and check for available updates.</li>
            <li>
              Click the Connector version checkbox and click Next and follow the
              instructions provided by the user interface.
            </li>
            <li>Restart Studio when prompted.</li>
            <li>
              After restarting, when creating a flow and using the connector, if
              you have several versions of the connector installed, you may be
              asked which version you would like to use. Choose the version you
              would like to use.
            </li>
          </ol>
          <p>
            Additionally, we recommend that you keep Studio up to date with its
            latest version.
          </p>
        </Card>
      </Layout>
    </>
  );
};

export default Prerequisites;
