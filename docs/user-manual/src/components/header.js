import React from "react";
import "./header.css";

const Header = () => {
  return (
    <div className="header">
      <h1 style={{ maxWidth: "1240px", margin: "0 auto" }}>
        JIRA Rest Connector - Mule 4
      </h1>
    </div>
  );
};

export default Header;
