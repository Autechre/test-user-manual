import React from "react";
import Layout from "./layout";

const Examples = () => {
  return <Layout>examples</Layout>;
};

export default Examples;
