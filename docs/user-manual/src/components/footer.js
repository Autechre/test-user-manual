import React from "react";
import "./footer.css";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="copyright">&copy; 2020 Hotovo s.r.o.</div>
      <div className="signature">All right reserved</div>
    </footer>
  );
};

export default Footer;
